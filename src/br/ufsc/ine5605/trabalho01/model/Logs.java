package br.ufsc.ine5605.trabalho01.model;

import java.io.Serializable;

public class Logs implements Serializable{
	private int codigo;
	private String data;
	private String status;
	private Funcionario funcionario;
	private Veiculo veiculo;
	private static final long serialVersionUID = 1L;
	
	public Logs(int codigo, String data,String status, Funcionario funcionario, Veiculo veiculo) {
		this.codigo = codigo;
		this.data = data;
		this.status = status;
		this.funcionario = funcionario;
		this.veiculo = veiculo;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Funcionario getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	
}
