package br.ufsc.ine5605.trabalho01.model;

import java.io.Serializable;

public class Veiculo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String placa;
	private String modelo;
	private String marca;
	private int ano;
	private int quilometragem;
	private boolean disponibilidade;
	
	public Veiculo(String placa, String modelo, String marca, int ano, int quilometragem) {
		super();
		this.placa = placa;
		this.modelo = modelo;
		this.marca = marca;
		this.ano = ano;
		this.quilometragem = quilometragem;
		this.disponibilidade = true;
	}
	
	public String getPlaca() {
		return this.placa;
	}
	
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public String getMarca() {
		return this.marca;
	}
	
	public String getModelo() {
		return this.modelo;
	}

	public int getAno() {
		return ano;
	}

	public int getQuilometragem() {
		return quilometragem;
	}

	public boolean isDisponibilidade() {
		return disponibilidade;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public void setQuilometragem(int quilometragem) {
		this.quilometragem = quilometragem;
	}

	public void setDisponibilidade(boolean disponibilidade) {
		this.disponibilidade = disponibilidade;
	}
	
	
	
	
}
