package br.ufsc.ine5605.trabalho01.persistencia;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.ufsc.ine5605.trabalho01.model.Veiculo;

public class VeiculoDAO {
	
	private HashMap<String, Veiculo> cacheVeiculos;
	private final String filename = "veiculo.cla";
	
	public VeiculoDAO() {
		this.cacheVeiculos = new HashMap<>();
		load();
	}

	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);

			this.cacheVeiculos = (HashMap<String, Veiculo>) oit.readObject();
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public void put(Veiculo veiculo) throws Exception {
		if (veiculo != null) {
			cacheVeiculos.put(veiculo.getPlaca(), veiculo);
			persist();
		}
	}

	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cacheVeiculos);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public Veiculo get(String placa) {
		return cacheVeiculos.get(placa);
	}

	public void remove(String placa) {
		cacheVeiculos.remove(placa);
		persist();
	}

	public Collection<Veiculo> getList() {
		return cacheVeiculos.values();
	}
}
