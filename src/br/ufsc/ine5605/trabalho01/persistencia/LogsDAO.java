package br.ufsc.ine5605.trabalho01.persistencia;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.ufsc.ine5605.trabalho01.model.Logs;

public class LogsDAO {
	private HashMap<Integer, Logs> cacheLogs;
	private final String filename = "logs.cla";
	
	public LogsDAO(){
		cacheLogs = new HashMap<>();
		load();
	}
	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);

			this.cacheLogs = (HashMap<Integer, Logs>) oit.readObject();
			
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	public void put(Logs logs) throws Exception{
		if(logs !=null){
			cacheLogs.put(logs.getCodigo(), logs);
			persist();
		}
		throw new Exception("Log nulo");
	}
	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cacheLogs);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	public Logs get(Integer codigo){
		return cacheLogs.get(codigo);
	}
	public void remove(Integer codigo){
		cacheLogs.remove(codigo);
		persist();
	}
	public Collection<Logs> getList(){
		return cacheLogs.values();
	}
}
