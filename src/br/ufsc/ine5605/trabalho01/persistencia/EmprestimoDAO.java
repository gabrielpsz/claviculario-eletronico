package br.ufsc.ine5605.trabalho01.persistencia;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

import br.ufsc.ine5605.trabalho01.control.Claviculario;
import br.ufsc.ine5605.trabalho01.model.Emprestimo;
import br.ufsc.ine5605.trabalho01.model.Funcionario;

public class EmprestimoDAO {
	private HashMap<Integer, Emprestimo> cacheEmprestimos;
	private final String filename = "emprestimos.cla";
	
	public EmprestimoDAO(){
		cacheEmprestimos = new HashMap<>();
		load();
	}

	@SuppressWarnings("unchecked")
	private void load() {
		try {
			FileInputStream fin = new FileInputStream(filename);
			ObjectInputStream oit = new ObjectInputStream(fin);

			this.cacheEmprestimos = (HashMap<Integer, Emprestimo>) oit.readObject();
			
			oit.close();
			fin.close();
			oit = null;
			fin = null;
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	public void put(Emprestimo emprestimo) throws Exception{
		if(emprestimo != null){
			cacheEmprestimos.put(emprestimo.getCodigo(), emprestimo);
			persist();
		}
	}

	public void persist() {
		try {
			FileOutputStream fout = new FileOutputStream(filename);
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(cacheEmprestimos);
			oos.flush();
			fout.flush();
			oos.close();
			fout.close();
			oos = null;
			fout = null;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	public Emprestimo get(Integer codigo){
		return cacheEmprestimos.get(codigo);
	}
	public void remove(Integer codigo){
		cacheEmprestimos.remove(codigo);
		persist();
	}
	public Collection<Emprestimo> getList(){
		return cacheEmprestimos.values();
	}
}
