package br.ufsc.ine5605.trabalho01.control;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;

import br.ufsc.ine5605.trabalho01.model.Veiculo;
import br.ufsc.ine5605.trabalho01.view.TelaAlugaVeiculo;
import br.ufsc.ine5605.trabalho01.view.TelaClav;
import br.ufsc.ine5605.trabalho01.view.TelaDevolveVeiculo;
import br.ufsc.ine5605.trabalho01.view.TelaEmprestimo;
import br.ufsc.ine5605.trabalho01.view.TelaListaEmprestimo;

public class Claviculario {

	private static Claviculario instance;
	private ControleEmprestimo ctrlEmprestimo = ControleEmprestimo.getInstance();
	private ControleFuncionario ctrlFuncionario = ControleFuncionario.getInstance();
	private ControleVeiculo ctrlVeiculo = ControleVeiculo.getInstance();
	private ControleLogs ctrlLogs = ControleLogs.getInstance();
	
	private TelaAlugaVeiculo telaAlugaVeiculo;
	private TelaEmprestimo telaEmprestimo;
	private TelaClav telaClav;
	private TelaDevolveVeiculo telaDevolveVeiculo;
	private TelaListaEmprestimo telaListaEmprestimo;
//	private ArrayList<String> logs;

	private Claviculario() {
		this.telaAlugaVeiculo = new TelaAlugaVeiculo(this);
		this.telaClav = new TelaClav(this);
		this.telaDevolveVeiculo = new TelaDevolveVeiculo(this);
		this.telaListaEmprestimo = new TelaListaEmprestimo(this);
//		this.logs = new ArrayList<>();
	}

	public static Claviculario getInstance() {
		if (instance == null) {
			instance = new Claviculario();
		}
		return instance;
	}

	public boolean possuiMaisQueUmVeiculo(int matricula) throws Exception {
		if (getCtrlFuncionario().funcionarioExiste(matricula)) {
			if (getCtrlFuncionario().encontraFuncionarioPorMatricula(matricula).getVeiculosPermitidos().size() >= 2) {
				return true;
			}
		}
		return false;
	}

	public void alugaVeiculo(int matricula, String placa) throws Exception {
		//verifica se funcionario tem o veiculo em sua lista
		if (ctrlFuncionario.funcionarioTemVeiculo(matricula, placa)) {
			//verifica se funcionario tem emprestimo
			if (!ctrlEmprestimo.verificaTemEmprestimoFuncionario(matricula)) {
				//verifica se veiculo tem emprestimo
				if (!ctrlEmprestimo.verificaTemEmprestimoVeiculo(placa)) {
					//verifica se eh diretor
					if (ctrlFuncionario.isDiretor(matricula)) {
						//inclui emprestimo, para funcionario e veiculo desejado
						ctrlEmprestimo.incluiEmprestimo(ctrlFuncionario.encontraFuncionarioPorMatricula(matricula),
								ctrlVeiculo.encontraVeiculoPorPlaca(placa));
//						geraLog(matricula, placa, 1);
						ctrlLogs.geraLog(1, ctrlFuncionario.encontraFuncionarioPorMatricula(matricula), ctrlVeiculo.encontraVeiculoPorPlaca(placa));
						//zera contador de tentativas de aluguel
						ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).setTentativaDeAcesso(0);
					} else {
						//inclui emprestimo, para funcionario e veiculo desejado
						ctrlEmprestimo.incluiEmprestimo(ctrlFuncionario.encontraFuncionarioPorMatricula(matricula),
								ctrlFuncionario.encontraVeiculoNoFuncionario(placa));
//						geraLog(matricula, placa, 1);
						ctrlLogs.geraLog(1, ctrlFuncionario.encontraFuncionarioPorMatricula(matricula), ctrlVeiculo.encontraVeiculoPorPlaca(placa));
						//zera contador de tentativas de aluguel
						ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).setTentativaDeAcesso(0);
					}
				} else {
//					geraLog(matricula, placa, 2);
					ctrlLogs.geraLog(2, ctrlFuncionario.encontraFuncionarioPorMatricula(matricula), ctrlVeiculo.encontraVeiculoPorPlaca(placa));
					ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).setTentativaDeAcesso(
							ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).getTentativaDeAcesso() + 1);
					throw new Exception("Veiculo j� est� alugado");
				}
			} else {
//				geraLog(matricula, placa, 2);
				ctrlLogs.geraLog(2, ctrlFuncionario.encontraFuncionarioPorMatricula(matricula), ctrlVeiculo.encontraVeiculoPorPlaca(placa));
				ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).setTentativaDeAcesso(
						ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).getTentativaDeAcesso() + 1);
				throw new Exception("Funcionario j� tem veiculo alugado");
			}
			// ctrlFuncionario.encontraVeiculoNoFuncionario(placa).setDisponibilidade(false);
		} else {
			ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).setTentativaDeAcesso(
					ctrlFuncionario.encontraFuncionarioPorMatricula(matricula).getTentativaDeAcesso() + 1);
//			geraLog(matricula, placa, 2);
			ctrlLogs.geraLog(2, ctrlFuncionario.encontraFuncionarioPorMatricula(matricula), ctrlVeiculo.encontraVeiculoPorPlaca(placa));
			throw new Exception("Funcionario n�o tem permiss�o ao veiculo: " + placa);
		}

	}

	public void devolveVeiculo(int matricula, String placa, int quilometragem) throws Exception {
		if (ctrlEmprestimo.verificaTemEmprestimoFuncionario(matricula)) {
			if (ctrlEmprestimo.verificaTemEmprestimoVeiculo(placa)) {
				if (ctrlEmprestimo.verificaEmprestimoMatriculaPlaca(matricula, placa)) {
					// ctrlFuncionario.encontraVeiculoNoFuncionario(placa).setDisponibilidade(true);
					Veiculo v = ctrlVeiculo.encontraVeiculoPorPlaca(placa);
					v.setQuilometragem(quilometragem);
					v.setDisponibilidade(true);
					ctrlVeiculo.getVeiculoDAO().put(v);
					ctrlVeiculo.getVeiculoDAO().persist();
					ctrlEmprestimo.removerEmprestimo(
							ctrlEmprestimo.encontraEmprestimoPorMatriculaPlaca(matricula, placa).getCodigo());
//					geraLog(matricula, placa, 3);
					ctrlLogs.geraLog(3, ctrlFuncionario.encontraFuncionarioPorMatricula(matricula), ctrlVeiculo.encontraVeiculoPorPlaca(placa));
				} else {
					throw new Exception("Funcionario " + matricula + " n�o alugou este veiculo");
				}
			} else {
				throw new Exception("Veiculo n�o est� alugado");
			}
		} else {
			throw new Exception("Funcionario n�o tem emprestimo");
		}
	}

//	public String geraLog(int matricula, String placa, int acesso) {
//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
//		String log = "";
//		if (acesso == 1) {
//			log += "Acesso permitido ao veiculo " + placa + ", as " + sdf.format(new Date()) + ". Matricula: "
//					+ matricula;
//		} else if (acesso == 2) {
//			log += "Acesso negado ao veiculo: " + placa + ", as " + sdf.format(new Date()) + ". Matricula: "
//					+ matricula;
//		} else if (acesso == 3) {
//			log += "Veiculo " + placa + " devolvido as " + sdf.format(new Date()) + ". Matricula: " + matricula;
//		}
//		this.logs.add(log);
//		return log;
//	}
//
//	public String listaLogs() throws Exception {
//		String lista = "";
//		int n = 1;
//		if (!this.logs.isEmpty()) {
//			for (String s : this.logs) {
//				lista += "-------------" + n + "-------------\n";
//				lista += s + "\n";
//				n++;
//			}
//
//		} else {
//			throw new Exception("Lista vazia");
//		}
//		return lista;
//	}
//
//	public String buscaLogPorMatricula(int matricula) {
//		String lista = "";
//		if (ctrlFuncionario.funcionarioExiste(matricula)) {
//			for (String s : this.logs) {
//				if (s.contains("Matricula: " + Integer.toString(matricula))) {
//					lista += s + "\n";
//				}
//			}
//		}
//		return lista;
//	}
//
//	public String buscaLogPorMotivo(String motivo) {
//		String lista = "";
//		for (String s : this.logs) {
//			if (s.contains(motivo)) {
//				lista += s + "\n";
//			}
//		}
//		return lista;
//	}
//
//	public String buscaLogPorPlaca(String placa) {
//		String lista = "";
//		for (String s : this.logs) {
//			if (s.contains(placa)) {
//				lista += s + "\n";
//			}
//		}
//		return lista;
//	}

	public void inicia() {
		//telaClaviculario.inicia();
		telaClav.setVisible(true);
	}

	public ControleEmprestimo getCtrlEmprestimo() {
		return ctrlEmprestimo;
	}

	public ControleFuncionario getCtrlFuncionario() {
		return ctrlFuncionario;
	}

	public ControleVeiculo getCtrlVeiculo() {
		return ctrlVeiculo;
	}
	public ControleLogs getCtrlLogs(){
		return ctrlLogs;
	}

	public boolean isEntradaInteiraValida(String entrada) throws Exception {
		boolean validade = false;
		for (int i = 0; i < entrada.length(); i++) {
			if (Character.isDigit(entrada.charAt(i))) {
				validade = true;
			} else {
				throw new InputMismatchException("A entrada deve conter apenas numeros.");
			}
		}
		return validade;
	}

	public void abreTelaAlugaVeiculo() {
		// TODO Auto-generated method stub
		telaAlugaVeiculo.setVisible(true);
	}
	
	public void abreTelaEmprestimo(int matricula) throws Exception{
		// TODO Auto-generated method stub
		if (matricula != 0) {
			if (ctrlFuncionario.funcionarioExiste(matricula)) {
				this.telaEmprestimo = new TelaEmprestimo(this);
				telaEmprestimo.pegaMatricula(matricula);
				telaEmprestimo.setVisible(true);
				telaEmprestimo.init();
			} else {
				throw new Exception("Matricula "+matricula +" nao existe");
			}
		}
	}

	public void abreTelaDevolveVeiculo() {
		telaDevolveVeiculo.setVisible(true);
	}

	public void abreTelaListaEmprestimo() {
		telaListaEmprestimo.updateData();
		telaListaEmprestimo.setVisible(true);
	}
}
