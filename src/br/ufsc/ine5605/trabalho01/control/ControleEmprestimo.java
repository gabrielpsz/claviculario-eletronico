package br.ufsc.ine5605.trabalho01.control;

import javax.swing.JOptionPane;

import br.ufsc.ine5605.trabalho01.model.Emprestimo;
import br.ufsc.ine5605.trabalho01.model.Funcionario;
import br.ufsc.ine5605.trabalho01.model.Veiculo;
import br.ufsc.ine5605.trabalho01.persistencia.EmprestimoDAO;

public class ControleEmprestimo {

	//private ArrayList<Emprestimo> emprestimos;
	private static ControleEmprestimo instance;
	private EmprestimoDAO emprestimoDAO;
	private int codigoEmprestimo = 0;

	public EmprestimoDAO getEmprestimoDAO(){
		if(emprestimoDAO == null){
			return new EmprestimoDAO();
		}
		return emprestimoDAO;
	}
	
	private ControleEmprestimo() {
		//this.emprestimos = new ArrayList<>();
		emprestimoDAO = new EmprestimoDAO();
	}

	public static ControleEmprestimo getInstance() {
		if (instance == null) {
			instance = new ControleEmprestimo();
		}
		return instance;
	}

	public void incluiEmprestimo(Funcionario funcionario, Veiculo veiculo) throws Exception {
		Emprestimo e = new Emprestimo(funcionario, veiculo);
		veiculo.setDisponibilidade(false);
		Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().put(veiculo);
		Claviculario.getInstance().getCtrlVeiculo().getVeiculoDAO().persist();
		++codigoEmprestimo;
		e.setCodigo(codigoEmprestimo);
		this.emprestimoDAO.put(e);
	}

	public boolean verificaTemEmprestimoFuncionario(int matricula) {
		if (matricula != 0) {
			if (Claviculario.getInstance().getCtrlFuncionario().funcionarioExiste(matricula)) {
				for (Emprestimo e : emprestimoDAO.getList()) {
					if (e.getFuncionario().getMatricula() == matricula) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean verificaTemEmprestimoVeiculo(String placa) {
		if (Claviculario.getInstance().getCtrlVeiculo().isPlacaValida(placa)) {
			for (Emprestimo e : emprestimoDAO.getList()) {
				if (e.getVeiculo().getPlaca().equals(placa)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean verificaEmprestimoMatriculaPlaca(int matricula, String placa) throws Exception{
		if (!placa.equals("") && placa != null) {
			if (Claviculario.getInstance().getCtrlFuncionario().funcionarioExiste(matricula)) {
				if(!emprestimoDAO.getList().isEmpty()){
					for (Emprestimo e : emprestimoDAO.getList()) {
						if (e.getFuncionario().getMatricula() == matricula && e.getVeiculo().getPlaca().equals(placa)) {
							return true;
						}
					}
				}
			} else {
				throw new Exception("Esta matricula nao existe.");
			}
		} else {
			throw new Exception("Digite uma placa valida");
		}
		return false;
	}

	public void removerEmprestimo(int codigo) {
		Emprestimo e = encontraEmprestimoPorCodigo(codigo);
		this.emprestimoDAO.remove(codigo);
	}

	public Emprestimo encontraEmprestimoPorCodigo(int codigo) {
		for (Emprestimo e : this.emprestimoDAO.getList()) {
			if (e.getCodigo() == codigo) {
				return e;
			}
		}
		return null;
	}

	public Emprestimo encontraEmprestimoPorMatriculaPlaca(int matricula, String placa) {
		if (verificaTemEmprestimoFuncionario(matricula) && verificaTemEmprestimoVeiculo(placa)) {
			for (Emprestimo e : emprestimoDAO.getList()) {
				if (e.getFuncionario().getMatricula() == matricula && e.getVeiculo().getPlaca().equals(placa)) {
					return e;
				}
			}
		}
		return null;
	}
	public Funcionario encontraFuncionarioPorVeiculo(String placa) {
		if (this.emprestimoDAO.getList().size() != 0) {
			for (Emprestimo e : this.emprestimoDAO.getList()) {
				if (e.getVeiculo().getPlaca().equals(placa)) {
					return e.getFuncionario();
				}
			}
		}
		return null;
	}
	public Veiculo encontraVeiculoPorMatricula(int matricula){
		for(Emprestimo e : this.emprestimoDAO.getList()){
			if(e.getFuncionario().getMatricula() == matricula){
				return e.getVeiculo();
			}
		}
		return null;
	}
	public String listarEmprestimos() {
		String lista = "";
		if (this.emprestimoDAO.getList().isEmpty()) {
			return "Lista vazia";
		}
		for (Emprestimo e : this.emprestimoDAO.getList()) {
			lista += "------------------------------------------\n";
			lista += "C�digo do emprestimo: " + e.getCodigo() + "\n";
			lista += "Placa do ve�culo: " + e.getVeiculo().getPlaca() + "\n";
			lista += "Funcionario respons�vel " + e.getFuncionario().getNome() + "\n";
			lista += "Matr�cula: " + e.getFuncionario().getMatricula() + "\n";
			lista += "------------------------------------------\n";
		}
		return lista;
	}

	public boolean existeEmprestimo(int codigo) {
		return false;
	}

	public String exibeEmprestimoPorMatricula(int matricula) {
		String lista = "";
		if (!emprestimoDAO.getList().isEmpty()) {
			for (Emprestimo e : emprestimoDAO.getList()) {
				if (e.getFuncionario().getMatricula() == matricula) {
					lista += "------------------------------------------\n";
					lista += "C�digo do emprestimo: " + e.getCodigo() + "\n";
					lista += "Placa do ve�culo: " + e.getVeiculo().getPlaca() + "\n";
					lista += "Funcionario respons�vel " + e.getFuncionario().getNome() + "\n";
					lista += "Matr�cula: " + e.getFuncionario().getMatricula() + "\n";
					lista += "------------------------------------------\n";
				}
			}
		} else {
			lista += "Lista vazia";
		}
		return lista;
	}

//	public ArrayList<Emprestimo> getEmprestimos() {
//		return this.emprestimos;
//	}
	

}
