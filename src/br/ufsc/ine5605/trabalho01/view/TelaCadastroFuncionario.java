package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufsc.ine5605.trabalho01.control.ControleFuncionario;
import br.ufsc.ine5605.trabalho01.model.Cargo;

public class TelaCadastroFuncionario extends JFrame {
	private ControleFuncionario ctrlFuncionario;
	private GerenciadorBotoesFunc gBtn;
	// componetes
	private JLabel lbNome;
	private JTextField tfNome;
	private JLabel lbDataDeNascimento;
	private JTextField tfDataDeNascimento;
	private JLabel lbTelefone;
	private JTextField tfTelefone;
	private JComboBox<Cargo> cbCargo;
	private String[] cargos = { Cargo.DIRETORIA.toString(), Cargo.SERVICOSGERAIS.toString(), Cargo.RH.toString(),
			Cargo.INFORMATICA.toString() };
	private JButton btOk;

	public TelaCadastroFuncionario(ControleFuncionario owner) {
		super("Tela cadastro funcionario");
		this.ctrlFuncionario = owner;
		this.gBtn = new GerenciadorBotoesFunc();
		init();
	}

	public void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// NOME
		lbNome = new JLabel();
		lbNome.setText("Nome");
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(lbNome, constraints);
		tfNome = new JTextField();
		tfNome.setText("");
		constraints.gridx = 1;
		constraints.gridy = 0;
		tfNome.setPreferredSize(new Dimension(100, 20));
		container.add(tfNome, constraints);
		// DATAdeNASC
		lbDataDeNascimento = new JLabel();
		lbDataDeNascimento.setText("Data de nascimento");
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(lbDataDeNascimento, constraints);
		tfDataDeNascimento = new JTextField();
		tfDataDeNascimento.setText("");
		constraints.gridx = 1;
		constraints.gridy = 1;
		tfDataDeNascimento.setPreferredSize(new Dimension(100, 20));
		container.add(tfDataDeNascimento, constraints);
		// TELEFONE
		lbTelefone = new JLabel();
		lbTelefone.setText("Telefone");
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(lbTelefone, constraints);
		tfTelefone = new JTextField();
		tfTelefone.setText("");
		constraints.gridx = 1;
		constraints.gridy = 2;
		tfTelefone.setPreferredSize(new Dimension(100, 20));
		container.add(tfTelefone, constraints);
		// cbCargo
		cbCargo = new JComboBox(cargos);
		constraints.gridx = 1;
		constraints.gridy = 3;
		container.add(cbCargo, constraints);
		// btOk
		btOk = new JButton();
		btOk.setActionCommand("Incluir");
		btOk.addActionListener(gBtn);
		// btOk.addActionListener();
		btOk.setText("Salvar!");
		constraints.gridx = 1;
		constraints.gridy = 4;
		container.add(btOk, constraints);
		// config JFrame
		setSize(550, 350);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	public void zeraCampos(){
		tfNome.setText("");
		tfDataDeNascimento.setText("");
		tfTelefone.setText("");
	}
	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Incluir")) {
				int matricula = ctrlFuncionario.geraMatricula(cbCargo.getSelectedIndex() + 1);
				try {
					ctrlFuncionario.recebeDadosTela(matricula, tfNome.getText(), tfDataDeNascimento.getText(),
							tfTelefone.getText(), cbCargo.getSelectedIndex() + 1);
					JOptionPane.showMessageDialog(null, "Funcionario cadastrado");
					zeraCampos();
					dispose();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
				
			}
		}

	}
}
