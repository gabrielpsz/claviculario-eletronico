package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufsc.ine5605.trabalho01.control.ControleVeiculo;

public class TelaCadastroVeiculo extends JFrame {

	private ControleVeiculo ctrlVeiculo;
	private GerenciadorBotoesFunc gBtn;

	private JLabel lbPlaca;
	private JTextField tfPlaca;
	private JLabel lbModelo;
	private JTextField tfModelo;
	private JLabel lbMarca;
	private JTextField tfMarca;
	private JLabel lbAno;
	private JTextField tfAno;
	private JLabel lbQuilometragem;
	private JTextField tfQuilometragem;

	private JButton btOk;

	public TelaCadastroVeiculo(ControleVeiculo owner) {
		super("Cadastro de veiculo");
		this.ctrlVeiculo = owner;
		this.gBtn = new GerenciadorBotoesFunc();
		init();
	}

	private void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		// Placa
		lbPlaca = new JLabel();
		lbPlaca.setText("Placa: ");
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(lbPlaca, constraints);
		tfPlaca = new JTextField();
		tfPlaca.setText("");
		constraints.gridx = 1;
		constraints.gridy = 0;
		tfPlaca.setPreferredSize(new Dimension(100, 20));
		container.add(tfPlaca, constraints);

		// Modelo
		lbModelo = new JLabel();
		lbModelo.setText("Modelo: ");
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(lbModelo, constraints);
		tfModelo = new JTextField();
		tfModelo.setText("");
		constraints.gridx = 1;
		constraints.gridy = 1;
		tfModelo.setPreferredSize(new Dimension(100, 20));
		container.add(tfModelo, constraints);
		// Marca
		lbMarca = new JLabel();
		lbMarca.setText("Marca: ");
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(lbMarca, constraints);
		tfMarca = new JTextField();
		tfMarca.setText("");
		constraints.gridx = 1;
		constraints.gridy = 2;
		tfMarca.setPreferredSize(new Dimension(100, 20));
		container.add(tfMarca, constraints);

		// Ano
		lbAno = new JLabel();
		lbAno.setText("Ano: ");
		constraints.gridx = 0;
		constraints.gridy = 3;
		container.add(lbAno, constraints);
		tfAno = new JTextField();
		tfAno.setText("");
		constraints.gridx = 1;
		constraints.gridy = 3;
		tfAno.setPreferredSize(new Dimension(100, 20));
		container.add(tfAno, constraints);

		// Quilometragem
		lbQuilometragem = new JLabel();
		lbQuilometragem.setText("Quilometragem: ");
		constraints.gridx = 0;
		constraints.gridy = 4;
		container.add(lbQuilometragem, constraints);
		tfQuilometragem = new JTextField();
		tfQuilometragem.setText("");
		constraints.gridx = 1;
		constraints.gridy = 4;
		tfQuilometragem.setPreferredSize(new Dimension(100, 20));
		container.add(tfQuilometragem, constraints);

		// btOk
		btOk = new JButton();
		btOk.setActionCommand("Incluir");
		btOk.addActionListener(gBtn);
		// btOk.addActionListener();
		btOk.setText("Salvar!");
		constraints.gridx = 1;
		constraints.gridy = 5;
		container.add(btOk, constraints);

		// config JFrame
		setSize(550, 350);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Incluir")) {
				int ano = 0;
				int quilometragem = 0;
				try {
					ano = Integer.parseInt(tfAno.getText());
					quilometragem = Integer.parseInt(tfQuilometragem.getText());
					try {
						ctrlVeiculo.recebeDadosCadastroTela(tfPlaca.getText(), tfModelo.getText(), tfMarca.getText(),
								ano, quilometragem);
						JOptionPane.showMessageDialog(null, "Veiculo cadastrado com sucesso!");
						apagarCampos();
						dispose();
						ctrlVeiculo.abreControleVeiculo();
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage());
					}
				} catch (NumberFormatException e2) {
					JOptionPane.showMessageDialog(null, "O ano e a quilometragem devem ser numeros inteiros.");
				}
			}
		}
		
		public void apagarCampos() {
			tfAno.setText("");
			tfMarca.setText("");
			tfModelo.setText("");
			tfPlaca.setText("");
			tfQuilometragem.setText("");
		}

	}
}
