package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import br.ufsc.ine5605.trabalho01.control.ControleFuncionario;

public class TelaGeralFuncionario extends JFrame{
	private GerenciadorBotoesGeralFunc gBtn;
	private ControleFuncionario ctrlFuncionario;
	//componentes
	private JButton btCadastro;
	private JButton btListar;
	private Dimension tamanhoBotao = new Dimension(150, 30);
	
	public TelaGeralFuncionario(ControleFuncionario owner){
		super("Tela geral funcionario");
		this.ctrlFuncionario = owner;
		this.gBtn = new GerenciadorBotoesGeralFunc();
		init();
	}
	private void init(){
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		
		//btCadastro
		btCadastro = new JButton();
		btCadastro.setText("Cadastro");
		btCadastro.setActionCommand("CadastroFuncionario");
		btCadastro.addActionListener(gBtn);
		btCadastro.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(btCadastro, constraints);
		//btListar
		btListar = new JButton();
		btListar.setText("Listar");
		btListar.setActionCommand("Listar");
		btListar.addActionListener(gBtn);
		btListar.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(btListar, constraints);
		
		//configJframe
		setSize(350, 250);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	private class GerenciadorBotoesGeralFunc implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("CadastroFuncionario")){
				ctrlFuncionario.telaCadastroFunc();
			}
			else if(e.getActionCommand().equals("Listar")){
				ctrlFuncionario.telaListarFunc();
			}
		}
		
	}
}
