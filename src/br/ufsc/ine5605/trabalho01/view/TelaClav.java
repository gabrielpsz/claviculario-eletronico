package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import br.ufsc.ine5605.trabalho01.control.Claviculario;

@SuppressWarnings("serial")
public class TelaClav extends JFrame {

	private GerenciadorBotoesFunc gBtn;
	private Claviculario claviculario;

	private JButton btAlugaVeiculo;
	private JButton btDevolveVeiculo;
	private JButton btListaEmprestimo;
	private JButton btControleVeiculo;
	private JButton btControleFuncionario;
	private JButton btListaLogs;
	
	private Dimension tamanhoBotao = new Dimension(150, 30);

	public TelaClav(Claviculario owner) {
		super("Claviculario eletronico");
		this.claviculario = owner;
		this.gBtn = new GerenciadorBotoesFunc();
		init();
	}

	private void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		// btAlugaVeiculo
		btAlugaVeiculo = new JButton();
		btAlugaVeiculo.setActionCommand("AlugaVeiculo");
		btAlugaVeiculo.addActionListener(gBtn);
		// btDevolveVeiculo.addActionListener();
		btAlugaVeiculo.setText("Alugar veiculo");
		btAlugaVeiculo.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 0;
		container.add(btAlugaVeiculo, constraints);

		// btDevolveVeiculo
		btDevolveVeiculo = new JButton();
		btDevolveVeiculo.setActionCommand("DevolveVeiculo");
		btDevolveVeiculo.addActionListener(gBtn);
		// btDevolveVeiculo.addActionListener();
		btDevolveVeiculo.setText("Devolver veiculo");
		btDevolveVeiculo.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 1;
		container.add(btDevolveVeiculo, constraints);

		// btListaEmprestimo
		btListaEmprestimo = new JButton();
		btListaEmprestimo.setActionCommand("ListaEmprestimo");
		btListaEmprestimo.addActionListener(gBtn);
		// btListaEmprestimo.addActionListener();
		btListaEmprestimo.setText("Listar emprestimos");
		btListaEmprestimo.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 2;
		container.add(btListaEmprestimo, constraints);

		// btControleVeiculo
		btControleVeiculo = new JButton();
		btControleVeiculo.setActionCommand("ControleVeiculo");
		btControleVeiculo.addActionListener(gBtn);
		// btControleVeiculo.addActionListener();
		btControleVeiculo.setText("Controle veiculo");
		btControleVeiculo.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 3;
		container.add(btControleVeiculo, constraints);

		// btControleFuncionario
		btControleFuncionario = new JButton();
		btControleFuncionario.setActionCommand("ControleFuncionario");
		btControleFuncionario.addActionListener(gBtn);
		// btOk.addActionListener();
		btControleFuncionario.setText("Controle funcionario");
		btControleFuncionario.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 4;
		container.add(btControleFuncionario, constraints);

		// btListaLogs
		btListaLogs = new JButton();
		btListaLogs.setActionCommand("ListaLogs");
		btListaLogs.addActionListener(gBtn);
		// btListaLogs.addActionListener();
		btListaLogs.setText("Lista de logs");
		btListaLogs.setPreferredSize(tamanhoBotao);
		constraints.gridx = 0;
		constraints.gridy = 5;
		container.add(btListaLogs, constraints);

		// config JFrame
		setSize(400, 350);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private class GerenciadorBotoesFunc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String opcao = e.getActionCommand();
			switch(opcao) {
			case "AlugaVeiculo":
				claviculario.abreTelaAlugaVeiculo();
				break;
			case "DevolveVeiculo":
				claviculario.abreTelaDevolveVeiculo();
				break;
			case "ListaEmprestimo":
				claviculario.abreTelaListaEmprestimo();
				break;
			case "ControleVeiculo":
				claviculario.getCtrlVeiculo().abreControleVeiculo();
				break;
			case "ControleFuncionario":
				claviculario.getCtrlFuncionario().inicia();
				break;
			case "ListaLogs":
				claviculario.getCtrlLogs().inicia();
				break;
			default:
				break;
			}
		}

	}
}
