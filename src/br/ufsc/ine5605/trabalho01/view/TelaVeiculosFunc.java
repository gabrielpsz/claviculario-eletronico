package br.ufsc.ine5605.trabalho01.view;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.ufsc.ine5605.trabalho01.control.ControleFuncionario;
import br.ufsc.ine5605.trabalho01.model.Veiculo;

public class TelaVeiculosFunc extends JFrame {
	private ControleFuncionario ctrlFuncionario;
	private GerenciadorBotoesTelaAdd gBtn;

	private DefaultTableModel tabelaV;
	private JTable tabelaVeiculosFuncionarios;
	private JScrollPane scroll;
	private JButton btVoltar;
	private JButton btAdicionar;
	private JButton btExcluir;
	private int mat;
	private String[] colunas = { "Placa", "Modelo", "Marca", "Ano", "Quilometragem", "Disponibilidade" };

	public TelaVeiculosFunc(ControleFuncionario owner) {
		super("Tela veiculos funcionario");
		this.ctrlFuncionario = owner;
		this.gBtn = new GerenciadorBotoesTelaAdd();
		this.tabelaV = new DefaultTableModel(colunas, 0);
		this.mat = 0;
	}

	public void init() {
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		// tabela
		container.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 12, 600, 300);
		container.add(scrollPane);

		tabelaVeiculosFuncionarios = new JTable();
		tabelaVeiculosFuncionarios.setModel(tabelaV);
		scrollPane.setViewportView(tabelaVeiculosFuncionarios);
		// btVoltar
		btVoltar = new JButton();
		btVoltar.setText("Voltar");
		btVoltar.setActionCommand("voltar");
		btVoltar.setBounds(10, 320, 150, 50);
		btVoltar.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 3;
		container.add(btVoltar, constraints);
		// btAdicionar
		btAdicionar = new JButton();
		btAdicionar.setText("Adicionar");
		btAdicionar.setActionCommand("adicionar");
		btAdicionar.setBounds(190, 320, 150, 50);
		btAdicionar.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 9;
		container.add(btAdicionar, constraints);
		// btExcluir
		btExcluir = new JButton();
		btExcluir.setText("Excluir");
		btExcluir.setActionCommand("excluir");
		btExcluir.setBounds(370, 320, 150, 50);
		btExcluir.addActionListener(gBtn);
		constraints.gridx = 0;
		constraints.gridy = 9;
		container.add(btExcluir, constraints);

		// config JFrame
		setSize(640, 500);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void updateData() {
		tabelaV.setNumRows(0);
		// if (ctrlFuncionario.getVeiculosFuncPorMatricula(mat).size() != 0) {
		for (Veiculo v : ctrlFuncionario.getVeiculosFuncPorMatricula(mat)) {
			String placa = v.getPlaca();
			String modelo = v.getModelo();
			String marca = v.getMarca();
			int ano = v.getAno();
			int quilometragem = v.getQuilometragem();
			boolean disponibilidade = v.isDisponibilidade();

			Object[] row = { placa, modelo, marca, ano, quilometragem, disponibilidade };
			tabelaV.addRow(row);
		}
		// }
	}

	public void pegaMatricula(int matricula) {
		mat = matricula;
	}

	public class GerenciadorBotoesTelaAdd implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("voltar")) {
				dispose();
			} else if (e.getActionCommand().equals("adicionar")) {
				ctrlFuncionario.telaAddVeiculos(mat);
			} else if (e.getActionCommand().equals("excluir")) {
				String placa = "";
				placa = JOptionPane.showInputDialog("Digite a placa do veiculo para excluir");
				if (placa != null) {
					try {

						ctrlFuncionario.removeVeiculo(placa, ctrlFuncionario.encontraFuncionarioPorMatricula(mat));
						updateData();
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(null, e1.getMessage());
					}
				}
			}
		}

	}
}
